public class Medicine {

    private final int medId;
    private final String medName;
    private final String medDesc;
    private final String medType;
    public String dosage;

    // Constructor
    Medicine( int medId, String medName, String medDesc, String medType, String dosage )
        {
            this.medId = medId;
            this.medName = medName;
            this.medDesc = medDesc;
            this.medType = medType;
            this.dosage  = dosage;
        }

    public Medicine(Medicine medicine) {
        this.medId = medicine.medId;
        this.medName = medicine.medName;
        this.medDesc = medicine.medDesc;
        this.medType = medicine.medType;
        this.dosage  = medicine.dosage;
    }


    public String getDosage()
    {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getMedName()
    {
        return medName;
    }

    public String getMedType()
    {
        return medType;
    }

    public String getMedDesc() { return medDesc; }
    public int getMedId() { return medId; }

}
