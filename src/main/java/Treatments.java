import java.time.LocalDate;
import java.util.TreeMap;

public class Treatments {

    TreeMap<LocalDate, String> treatmeants = new TreeMap();

    public void addTreatment() {
        var scanner = new BScanner();

        System.out.println("Please enter the treatment date (yyyy-MM-dd)");
        LocalDate date = scanner.scanDate();
        System.out.println("Please enter the treatment");
        String contents = scanner.scanString();
        treatmeants.put(date, contents);

    }

    public void printTreatments() {
        if (treatmeants.isEmpty()) {
            System.out.println("This patient don't have treatments yet."); //TODO: Add patient name to this function
        } else {
            for (var p : treatmeants.entrySet()) {
                System.out.format("[%s] | Treatment: %s\n", p.getKey().toString(), p.getValue());
            }
        }
    }

    public void removeTreatment() { //TODO: Add press 0 to get back
        if (treatmeants.isEmpty()){
            System.out.println("This patient don't have treatments!");
        }
        else {
            printTreatments();
            System.out.println("You can delete a treatment by typing in the treatment date (yyyy-MM-dd):");
            var choice = new BScanner();
            treatmeants.remove(choice.scanDate());
        }
    }
}
