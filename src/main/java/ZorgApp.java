import java.rmi.UnexpectedException;

public class ZorgApp {

    public static void main(String[] args) {

                new Administration();
                int patientId; // patientnr. 0=zorgverlener.
                System.out.println("Please login with your patient number:");
                Caretaker caretaker = new Caretaker();
                patientId = Administration.inputPatientId();

                if (patientId < 0){ // Error - no patient available with id < 0 TODO: Validate input if < 0 then again until valid Id
                    System.out.println("This patient number is not valid!");
                }

                else if (patientId == 0) {
                    caretaker.selectCaretaker();
                }

                else {
                    Patient.menu(patientId);

                }
            }
        }

// Using .json file TODO: Add .json file: patients/medicines
// Import .jar file TODO: Add .jar file to read .json file (or .gson)