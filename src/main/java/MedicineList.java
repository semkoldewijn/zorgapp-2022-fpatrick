import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

class MedicineList {
    private List<Medicine> medicines = new ArrayList<>();

    MedicineList() {
        medicines.add(new Medicine(1,"Asprine", "Helpt tegen de hoofdpijn", "pil", "500mg"));
        medicines.add(new Medicine(2,"Ibuprofen", "Helpt tegen onstekking", "pil", "40mg, 2x per dag"));
        medicines.add(new Medicine(2,"Paracetamol", "Helpt tegen buikpijn", "pil", "250"));
        medicines.add(new Medicine(2,"Neusspray", "Helpt tegen onstekking", "spray", "20gram, 3x per dag"));
    }

    public List<Medicine> getMedicines() {
        return medicines;
    }
}
