public class Pharmacist extends Caretaker {

    @Override
    void menu(int patientId) {

        final int PRINTMEDICINES = 4;
        final int EDITMEDICINES = 5;

        while (true) { //TODO: Ask, is this the right way? How can i do diffrent.
            super.menu(patientId);
            System.out.println("------------ Medicine ------------- ");
            System.out.format("%d:  Print medicine information\n", PRINTMEDICINES);
            System.out.format("%d:  Edit medicine information\n", EDITMEDICINES);
            System.out.println( "--------------------------------- " ); //12- | 8 | -13 = 33//

            System.out.println("Please make a choice:");
            BScanner scanner = new BScanner();
            int choice = scanner.scanInt();

            if (choice == 0 ||
                    choice == 1 ||
                    choice == 2 ||
                    choice == 3 ||
                    choice == PRINTMEDICINES ||
                    choice == EDITMEDICINES) {
                makeMenuChoice(choice);
            }
            else {
                System.out.println("Not a valid choice!");
            }
        }
    }
}


