public class Doctor extends Caretaker{

    @Override
    void menu(int patientId) {

        final int PRINTMEDICINES = 4;
        final int EDITMEDICINES = 5;
        final int PRINTWEIGHTMEASUREMENTS = 6;
        final int PRINTBMIMEASUREMENTS = 7;
        final int CHANGEMEASUREMENTS = 8;
        final int PRINTTREATMENTS = 9;
        final int ADDTREATMENTS = 10;
        final int REMOVETREATMENTS = 11;
        final int ADD = 12;

        while (true) { //TODO: Ask, is this the right way? How can i do diffrent.
            super.menu(patientId);
            System.out.println( "------------ Medicine ------------- " ); //12- | 8 | -13 = 33//
            System.out.format( "%d:  Print medicine information\n", PRINTMEDICINES );
            System.out.format( "%d:  Edit medicine information\n", EDITMEDICINES );
            System.out.println( "----------- Measurement ----------- " ); //11- | 11 | -11 = 33//
            System.out.format( "%d:  Print weight measurement(s)\n", PRINTWEIGHTMEASUREMENTS );
            System.out.format( "%d:  Print BMI measurement(s)\n", PRINTBMIMEASUREMENTS );
            System.out.format( "%d:  Edit measurement(s)\n", CHANGEMEASUREMENTS );
            System.out.println( "----------- Treatments ------------ " ); //11- | 10 | -12 = 33//
            System.out.format( "%d:  Print treatment(s)\n", PRINTTREATMENTS );
            System.out.format( "%d:  Add treatment\n", ADDTREATMENTS );
            System.out.format( "%d:  Remove treatment\n", REMOVETREATMENTS );
            System.out.println( "------------ Doctor ------------- " ); //12- | 8 | -13 = 33//
            System.out.format( "%d:  Add patient\n", ADD );
            System.out.println( "--------------------------------- " ); //12- | 8 | -13 = 33//

            System.out.println("Please make a choice:");
            BScanner scanner = new BScanner();
            int choice = scanner.scanInt();
            if (choice == 0 ||
                    choice == 1 ||
                    choice == 2 ||
                    choice == 3 ||
                    choice == PRINTMEDICINES ||
                    choice == EDITMEDICINES ||
                    choice == PRINTWEIGHTMEASUREMENTS ||
                    choice == PRINTBMIMEASUREMENTS ||
                    choice == CHANGEMEASUREMENTS ||
                    choice == PRINTTREATMENTS ||
                    choice == ADDTREATMENTS ||
                    choice == REMOVETREATMENTS ||
                    choice == ADD) {
                makeMenuChoice(choice);
            }
            else {
                System.out.println("Not a valid choice!");
            }
        }
    }
}





